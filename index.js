//disable deprecation msg
process.env['NTBA_FIX_350'] = 1;

const TelegramBot = require('node-telegram-bot-api');
require('dotenv').config();
const token = process.env.botToken;
const bot = new TelegramBot(token, { polling: true });
const fs = require('fs');
const ytdl = require('ytdl-core');
const ffmpeg = require('fluent-ffmpeg');
const { log } = require('console');

console.log('bot online');

bot.onText(/\/start/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(
    chatId,
    'Пожалуйста, вставьте в чат ссылку с ютуба, что бы получить mp3'
  );
});

bot.on('message',  async (msg) => {
  const chatId = msg.chat.id;
  let messageText = msg.text;

  if (messageText.includes('youtube.com') || messageText.includes('youtu.be')) {
    //sanitization to avoid polling err when link shared through YT android app
    if (messageText.includes('\n')) {
      let sanitizedString = messageText.split('\n')[1];
      messageText = sanitizedString;
      }      
    const videoId = ytdl.getURLVideoID(messageText);
    const downloadLink = `https://www.youtube.com/watch?v=${videoId}`;
    let songInfo = await ytdl.getInfo(downloadLink)

    bot.sendMessage(
      chatId,
      'Звуковая дорожка вырезается и конвертируется в mp3, это может занять несколько минут...'
    );

      const mp3DownloadPath = `./aidop_${videoId}.mp3`;
      const mp3DownloadStream = ytdl(downloadLink, {
        filter: (format) => format.container === 'mp4',
        quality: 'highestaudio',
      });
      const mp3FileStream = fs.createWriteStream(mp3DownloadPath);

      mp3DownloadStream.pipe(mp3FileStream);

      mp3DownloadStream.on('end', () => {
        const mergedFilePath = `./merged_${videoId}.mp3`;
        const command = ffmpeg()
          .input(mp3DownloadPath)
          .output(mergedFilePath)
          .on('end', () => {
            const audioData = fs.readFileSync(mergedFilePath);

            const fileOptions = {
              // Explicitly specify the file name.
              filename:  songInfo.videoDetails.title,
              // Explicitly specify the MIME type.
              // contentType: 'audio/mpeg', --- not used
            };
      
            bot.sendAudio(chatId, audioData, {
              caption: 'Получите, распишитесь!',
            }, fileOptions);            
            fs.unlinkSync(mp3DownloadPath);
            fs.unlinkSync(mergedFilePath);
          })
          .on('error', (error) => {
            console.error('Error merging files: ', error);
            bot.sendMessage(chatId, 'An error while merging the files');            
            fs.unlinkSync(mp3DownloadPath);
            fs.unlinkSync(mergedFilePath);
          })
          .run();
      });

      mp3DownloadStream.on('error', (error) => {
        console.error('Error downloading audio: ', error);
        bot.sendMessage(chatId, 'An error while donloading the audio');
      });
  }
});


      // //get ID from youtu.be -- unnecessary since this is build in fun but anyway :)
      // if(messageText.includes('youtu.be')){
      //   function youtube_parser(url){
      //     const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
      //     const match = url.match(regExp);
      //     return (match&&match[7].length==11)? match[7] : false;
      //   }
      //   console.log(youtube_parser(messageText));        
      // }